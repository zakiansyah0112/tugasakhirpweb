<!DOCTYPE html>
<html>
    <head>
        <script>
            function tanya(){
                if(confirm("Apakah anda yakin akan menghapus pendaftaran ini?")){
                    return true;
                }
                else{
                    return false;
                }
            }
        </script>
        <title>Pendaftaran EO | Zakiansyahh</title>
        <link rel="stylesheet" type="text/css" href="form_pendaftaran.css">
    </head>
    <body>
        <div class="warp">
            <div class="container">
                <h1>Form Pendaftaran</h1>
                <form action="hasil.php" method="POST">
                    <table>
                        <tr>
                            <td>Nama Lengkap</td>
                            <td></td>
                            <td><input type="text" name="nama_lengkap"></td>
                        </tr>
                        <tr>
                            <td>Tempat Lahir</td>
                            <td></td>
                            <td><input type="text" name="tempat_lahir"></td>
                        </tr>
                        <tr>
                            <td>Tanggal Lahir</td>
                            <td></td>
                            <td><input type="date" name="tanggal_lahir"></td>
                        </tr>
                        <tr>
                            <td>Jenis Kelamin</td>
                            <td></td>
                            <td>
                                <select name="jk">
                                <center>
                                    <option selected="Select">Pilih</option>
                                </center>
                                <option value="Laki-Laki">Laki-laki</option>
                                <option value="Perempuan">Perempuan</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>Agama</td>
                            <td></td>
                            <td>
                                <select name="info">
                                    <center>
                                        <option selected="Select">Pilih</option>
                                    </center>
                                        <option value="Islam">Islam</option>
                                        <option value="Katolik">Katolik</option>
                                        <option value="Kristen">Kristen</option>
                                        <option value="Hindu">Hindu</option>
                                        <option value="Budha">Budha</option>
                                        <option value="Konghucu">Konghucu</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>Email</td>
                            <td></td>
                            <td><input type="text" name="email"></td>
                        </tr>
                        <tr>
                            <td>Sosial Media </td>
                            <td></td>
                            <td>
                               <select name="sosmed">
                               <center>
                                    <option selected="Select">Pilih</option>
                                </center>
                                <option value="Facebook">Facebook</option>
                                <option value="Instagram">Instagram</option>
                                <option value="Twitter">Twitter</option>
                               </select>
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td>
                                <button type="submit" name="kirim">Kirim</button>
                                <button type="reset" name="reset">Reset</button>
                            </td>
                        </tr>
                    </table>
                </form>
            </div>
        </div>
    </body>
</html>